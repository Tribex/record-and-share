// COPYRIGHT (c) 2015 Joshua Bemenderfer
'use strict';

// gulpfile.js - Handles building of client CSS and JavaScript resources.
let gulp = require('gulp');
let livereload = require('gulp-livereload');

let webpack = require('webpack');
let gulpWebpack = require('gulp-webpack');
let less = require('gulp-less');
let autoprefixer = require('gulp-autoprefixer');
let minifyCSS = require('gulp-minify-css');

// Compiles JavaScript into one file using Webpack.
gulp.task('compile-js', function() {
  gulp.src(`${__dirname}/src/js/recordingApp.js`)
  .pipe(gulpWebpack({
    output: {
      filename: 'recordingApp-compiled.js',
    },
    //plugins: [new webpack.optimize.UglifyJsPlugin()],
  }))
  .pipe(gulp.dest(`${__dirname}/resources/scripts/`))
  .pipe(livereload());

  gulp.src(`${__dirname}/src/js/playbackApp.js`)
  .pipe(gulpWebpack({
    output: {
      filename: 'playbackApp-compiled.js',
    },
    //plugins: [new webpack.optimize.UglifyJsPlugin()],
  }))
  .pipe(gulp.dest(`${__dirname}/resources/scripts/`))
  .pipe(livereload());
});

gulp.task('compile-less', function() {
  gulp.src(`${__dirname}/src/less/*.less`)
  .pipe(less({
    paths: [`${__dirname}/src/less`],
  }))
  .pipe(autoprefixer({
    browsers: ['last 4 versions', 'IE 8', 'Android >= 3'],
    cascade: false,
  }))
  .pipe(minifyCSS())
  .pipe(gulp.dest(`${__dirname}/resources/styles/`))
  .pipe(livereload());
});

gulp.task('default', function() {
  livereload.listen({
    'basePath': './',
  });
  gulp.watch(`${__dirname}/src/less/**/*.*`, ['compile-less']);
  gulp.watch(`${__dirname}/src/js/**/*.*`, ['compile-js']);
});
