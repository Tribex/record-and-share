// COPYRIGHT (c) Joshua Bemenderfer 2015

var app = angular.module('RecordAndShareApp', ['ngMaterial', 'timer'])
.config(function($mdGestureProvider) {
  $mdGestureProvider.skipClickHijack();
});

// Load Services
require('./services/state-service')(app);
require('./services/recording-service')(app);
// Load Components
require('./controllers/recently-uploaded-controller')(app);
require('./controllers/sidebar-controller')(app);
require('./controllers/playback-controller')(app);

app.config(function($mdThemingProvider) {
  $mdThemingProvider.theme('default')
    .primaryPalette('amber')
    .accentPalette('green');
});

var calculateVideoWidth = function() {
  var videoElement = document.getElementById('videoDisplay');
  var controlsElement = document.querySelector('#mediaControls .inner');
  videoElement.width = window.innerWidth;
  videoElement.height = window.innerHeight-64;
};

window.addEventListener('resize', calculateVideoWidth);
calculateVideoWidth();
