// COPYRIGHT (c) Joshua Bemenderfer 2015

var app = angular.module('RecordAndShareApp', ['ngMaterial', 'timer'])
.config(function($mdGestureProvider) {
  $mdGestureProvider.skipClickHijack();
});

// Load Services
require('./services/state-service')(app);
require('./services/recording-service')(app);
// Load Components
require('./controllers/recently-uploaded-controller')(app);
require('./controllers/sidebar-controller')(app);
require('./controllers/recorder-controller')(app);
require('./controllers/upload-dialog-controller')(app);

app.controller('DialogController', [
  '$scope', 'StateService', '$mdDialog', '$mdMedia',
  function($scope, StateService, $mdDialog, $mdMedia) {

    $scope.showUploadDialog = function(ev) {
      var useFullScreen = $mdMedia('xs') || $mdMedia('sm');
      $mdDialog.show({
        controller: 'UploadDialogController',
        templateUrl: 'views/components/upload-dialog.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose: false,
        fullscreen: useFullScreen,
      })
      .then(function(answer) {
        $scope.status = 'You said the information was "' + answer + '".';
      }, function() {
        $scope.status = 'You cancelled the dialog.';
      });
    };

    $scope.$on('stateChange', function(channel, message) {
      if(message === 'hasRecorded') {
        $scope.showUploadDialog();
      }
    });

  }]
);

app.config(function($mdThemingProvider) {
  $mdThemingProvider.theme('default')
    .primaryPalette('amber')
    .accentPalette('green');
});

var calculateVideoWidth = function() {
  var videoElement = document.getElementById('videoDisplay');
  videoElement.width = window.innerWidth;
  videoElement.height = window.innerHeight-64;
};

window.addEventListener('resize', calculateVideoWidth);
calculateVideoWidth();
