// COPYRIGHT (c) Joshua Bemenderfer 2015

var controllerFactory = function(app) {

  app.controller('PlaybackController', ['$scope', '$http', 'StateService', 'RecordingService', function($scope, $http, StateService, RecordingService) {
    $scope.recording = null;
    $scope.error = false;
    $scope.mediaFormat = null;
    $scope.controlState = 'stopped';
    $scope.isFramed = (window.top !== window.self);
    $scope.recordingUrl = window.location.href.split('?')[0];

    var splitLocation = window.location.href.split('/');
    var indexOfRecording = splitLocation.indexOf('recording');
    var query = splitLocation[indexOfRecording+1].split('?')[1];
    var recordingId = splitLocation[indexOfRecording+1].split('?')[0];

    $scope.hasRecorded = query && query.length && query.indexOf('hasRecorded=true') !== -1;

    var controlsElement = null;
    $http.get('/api/v1/recording/'+recordingId).then(function(data) {
      $scope.recording = data.data;

      // Setup copy button.
      new window.Clipboard('.copy-button');

      controlsElement = document.getElementById('videoDisplay');

      // Lazy error check to switch the video to WebM if Mp4 isn't supported.
      controlsElement.addEventListener('error', function() {
        if($scope.mediaFormate !== 'WebM') {
          $scope.setMediaFormat('WebM');
        } else {
          $scope.setMediaFormat('MP4');
        }
      }, true);

      $scope.setMediaFormat(window.chrome ? 'WebM' : 'MP4');

      controlsElement.addEventListener('ended', function() {
        $scope.$broadcast('timer-start');
        $scope.$broadcast('timer-stop');
        $scope.controlState = 'stopped';
        $scope.$digest();
      });

      //$scope.playRecording();
    }, function(err) {
      console.log(err.status);
      $scope.error = err.data.error || err.status;
    });

    $scope.setMediaFormat = function(format) {
      $scope.mediaFormat = format;
      if(format === 'WebM') {
        controlsElement.src = '/api/v1/recording/'+recordingId+'/recording.webm';
      } else {
        controlsElement.src = '/api/v1/recording/'+recordingId+'/recording.mp4';
      }
    };

    $scope.playRecording = function() {
      $scope.$broadcast($scope.controlState === 'paused' ? 'timer-resume' : 'timer-start');
      $scope.controlState = 'playing';
      controlsElement.play();
    };

    $scope.stopRecording = function() {
      $scope.$broadcast('timer-start');
      $scope.$broadcast('timer-stop');
      $scope.controlState = 'stopped';
      controlsElement.currentTime = 0;
      controlsElement.pause();
    };

    $scope.pauseRecording = function() {
      $scope.$broadcast('timer-stop');
      $scope.controlState = 'paused';
      controlsElement.pause();
    };

    $scope.insertRecording = function() {
      $scope.recording.fullUrl = window.location.href;
      // Tell the embedding page that the user has requested to insert the recording into itself.
      window.top.postMessage({channel: 'recording.insertRecording', recording: $scope.recording}, '*');
    };

    $scope.goHome = function() {
      window.location = '/';
    };
  }]);
};

module.exports = controllerFactory;
