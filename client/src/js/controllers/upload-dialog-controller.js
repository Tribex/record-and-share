// COPYRIGHT (c) Joshua Bemenderfer 2015

var controllerFactory = function(app) {
  app.controller('UploadDialogController', [
    '$scope', 'RecordingService', 'StateService', '$mdDialog', '$mdMedia', '$timeout',
    function($scope, RecordingService, StateService, $mdDialog, $mdMedia, $timeout) {
      console.log('Initialized');
      var recordings = RecordingService.getRecordings();
      var recording = recordings[recordings.length-1];
      $scope.recording = recording;
      $scope.$mdMedia = $mdMedia;
      $scope.isFramed = (window.top !== window.self);

      $timeout(function() {
        new window.Clipboard('.copy-button');
      }, 100);

      var checkInterval = setInterval(function() {
        $scope.$digest();
      }, 200);

      $scope.openRecording = RecordingService.openRecording;

      $scope.insertRecording = function() {
        // Tell the embedding page that the user has requested to insert the recording into itself.
        window.top.postMessage({channel: 'recording.insertRecording', recording: $scope.recording}, '*');
      };

      $scope.deleteRecording = function(recordingToDelete, cancelExt) {
        RecordingService.deleteRecording(recordingToDelete);
        $mdDialog.cancel();
        StateService.setState('default');
        clearInterval(checkInterval);

        if(cancelExt) {
          // Tell the embedding page to close itself because the user has requested to fully cancel the operation.
          window.top.postMessage({channel: 'recording.fullCancel', recording: $scope.recording}, '*');
        }
      };

      $scope.cancel = function() {
        // Tell the embedding page that we just canceled the recording.
        window.top.postMessage({channel: 'recording.dialogCancel', recording: $scope.recording}, '*');

        if(!$scope.recording.uploaded) {
          RecordingService.deleteRecording($scope.recording);
        }
        $mdDialog.cancel();
        StateService.setState('default');
        clearInterval(checkInterval);
      };

      // TODO: Implement
      $scope.upload = function() {
        RecordingService.uploadRecording($scope.recording);
        StateService.setState('upload');
        clearInterval(checkInterval);
      };
    }]
  );
};

module.exports = controllerFactory;
