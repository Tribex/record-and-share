// COPYRIGHT (c) Joshua Bemenderfer 2015

var controllerFactory = function(app) {

  app.controller('RecentlyUploadedController', ['RecordingService', '$http', function(RecordingService, $http) {
    this.recordings = RecordingService.getRecordings();

    this.clearList = RecordingService.clearList;

    this.openRecording = RecordingService.openRecording;

    this.deleteRecording = RecordingService.deleteRecording;
  }]);
};

module.exports = controllerFactory;
