// COPYRIGHT (c) Joshua Bemenderfer 2015

var VideoRecorder = require('./recorders/video');
var AudioRecorder = require('./recorders/audio');

var controllerFactory = function(app) {

  app.controller('RecorderController', ['StateService', 'RecordingService', function(StateService, RecordingService) {
    var $scope = this;
    $scope.isRecording = false;
    $scope.recordingType = null; // 'audio' | 'video'
    $scope.recordings = RecordingService.getRecordings();
    $scope.isFramed = (window.top !== window.self);
    $scope.isOnIOS = false;

    $scope.currentRecording;

    var videoElement = document.getElementById('videoDisplay');
    var offscreenCanvas = document.getElementById('offscreenCanvas');
    var videoRecorder, audioRecorder;

    // Handle any recording preparation logic here.
    var successCallback = function(stream) {
      $scope.currentRecording = RecordingService.createRecording($scope.recordingType);
      $scope.isRecording = true;


      RecordingService.prepareUploadRecording($scope.currentRecording,
        // TODO: More robust error handling.
        function(recording, status) {
          if(!recording) {
            $scope.error = status;
          }
        });

      audioRecorder = new AudioRecorder(stream);

      if($scope.recordingType === 'video') {
        videoRecorder = new VideoRecorder(stream);
      }

      audioRecorder.initialize(function() {
        if(videoRecorder) {
          videoRecorder.initialize(function() {
            // Both recorders are ready to record things accurately
            videoRecorder.start();
            audioRecorder.start();
          });
        } else {
          audioRecorder.start();
        }
      });

      if($scope.recordingType === 'video') {
        videoElement.addEventListener('loadeddata', function() {
          // Draw thumbnail, scaled to 100x100 px.
          offscreenCanvas.width = 100;
          offscreenCanvas.height = 100;
          var context = offscreenCanvas.getContext('2d');
          context.drawImage(videoElement, 0, 0, 100, 100);
          $scope.currentRecording.snapshot = offscreenCanvas.toDataURL();
        });
      }

      videoElement.src = window.URL.createObjectURL(stream);
    };

    var errorCallback = function() {
      $scope.error = 'Unable to access camera.';
    };

    $scope.record = function(type) {
      if(Modernizr.getusermedia) {
        var getUserMedia = Modernizr.prefixed('getUserMedia', navigator);

        var options = {
          video: type === 'video',
          audio: true,
        };

        $scope.recordingType = type;
        getUserMedia(options, successCallback, errorCallback);
      }
    };

    $scope.stop = function() {
      $scope.isRecording = false;
      videoElement.src = '';

      RecordingService.addRecording($scope.currentRecording);
      StateService.setState('hasRecorded');
      var saveRecording = function(type, uri, blob) {
        $scope.currentRecording[type] = {
          uri: uri,
          blob: blob,
        };

        if($scope.recordingType === 'audio' || $scope.currentRecording['video'].blob && $scope.currentRecording['audio'].blob) {
          RecordingService.uploadRecording($scope.currentRecording);
        }
      };

      if($scope.recordingType === 'video') {
        videoRecorder.stop(function (videoURI) {
          var videoBlob = videoRecorder.getBlob();

          saveRecording('video', videoURI, videoBlob);
        });
      }

      audioRecorder.stop(function (audioURI) {
        var audioBlob = audioRecorder.getBlob();

        saveRecording('audio', audioURI, audioBlob);
      });
    };
  }]);
};

module.exports = controllerFactory;
