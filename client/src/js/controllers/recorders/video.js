
var VideoRecorder = function(stream, options) {
  options = options ? options : {};

  options.type = 'video';
  options.mimeType = 'video/webm';
  options.videoBitsPerSecond = 128000;
  options.bufferSize = 16384;
  options.width = 640;
  options.height = 480;


  this.recorder = RecordRTC(stream, options);

  // TODO = Flesh these out as needed.
  this.initialize = this.recorder.initRecorder;
  this.start = this.recorder.startRecording;
  this.stop = this.recorder.stopRecording;

  this.getBlob = this.recorder.getBlob;
  this.getDataURL = this.recorder.getDataURL;
};

module.exports = VideoRecorder;
