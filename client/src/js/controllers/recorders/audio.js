
var AudioRecorder = function(stream, options) {
  options = options ? options : {};

  options.type = 'audio';
  options.mimeType = 'audio/wav';
  options.audioBitsPerSecond = 128000;
  options.bufferSize = 16384;
  options.numberOfAudioChannels = 1;

  this.recorder = RecordRTC(stream, options);

  // TODO: Flesh these out as needed.
  this.initialize = this.recorder.initRecorder;
  this.start = this.recorder.startRecording;
  this.stop = this.recorder.stopRecording;

  this.getBlob = this.recorder.getBlob;
  this.getDataURL = this.recorder.getDataURL;
};

module.exports = AudioRecorder;
