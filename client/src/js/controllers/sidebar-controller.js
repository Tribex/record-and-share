// COPYRIGHT (c) Joshua Bemenderfer 2015

var controllerFactory = function(app) {

  app.controller('SidebarContoller', ['$mdSidenav', function($mdSidenav) {
    this.isOpen = false;

    this.toggleOpen = function() {
      $mdSidenav('left').toggle();
      this.isOpen = !this.isOpen;
    };
  }]);
};

module.exports = controllerFactory;
