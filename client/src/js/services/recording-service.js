// COPYRIGHT (c) Joshua Bemenderfer

var serviceFactory = function(app) {
  app.service('RecordingService', ['$http', function($http) {
    var recordings = [];

    try {
      var storedRecordings = JSON.parse(localStorage.getItem('recordings'));
      if(Array.isArray(storedRecordings)) {
        recordings = storedRecordings;
      }
    } catch (e) {
      console.log('Resetting Local Storage');
    }

    var getRecordings = function() {
      return recordings;
    };

    var addRecording = function(recording) {
      recordings.push(recording);
    };

    var deleteRecording = function(recording) {
      if(recording.uploaded) {
        $http({
          url: '/api/v1/recording/'+recording.deleteId,
          method: 'DELETE',
        }).then(function(response) {
          if(response.status === 200 && response.data && response.data.status === 'success') {
            recordings.splice(recordings.indexOf(recording), 1);
            storeLocally();
          }
        }, function(response) {
          // If we get a 404, then the recording didn't exist in the first place, so delete it locally.
          if(response.status === 404) {
            recordings.splice(recordings.indexOf(recording), 1);
            storeLocally();
          }
        });
      }
    };

    var storeLocally = function() {
      localStorage.setItem('recordings', JSON.stringify(recordings));
    };

    var clearList = function() {
      recordings.length = 0;
      storeLocally();
    };

    // TODO: Implement preparation logic.
    var prepareUploadRecording = function(recording, callback) {
      $http({
        url: '/api/v1/recording',
        method: 'POST',
        data: {
          name: recording.name,
          type: recording.type,
          snapshot: recording.snapshot,
          date: recording.date,
        },
      }).then(function(response) {
        if(response.status === 200) {
          recording.id = response.data.id;
          recording.deleteId = response.data.deleteId;
          recording.fullUrl = (window.location + getRecordingUrl(recording)).split('?')[0]
          // Ehehe...
          .replace('//recording', '/recording');

          window.top.postMessage({channel: 'recording.idReady', recording: recording}, '*');

          callback ? callback(recording) : null;
        } else {
          callback ? callback(false, response.status) : null;
        }
      }, function(errResponse) {
        callback ? callback(false, response.status) : null;
      });
    };

    var uploadRecording = function(recording, callback) {
      window.top.postMessage({channel: 'recording.creationStarted', recording: recording}, '*');
      var xhr = new XMLHttpRequest();
      xhr.timeout = 14400000;

      xhr.ontimeout = function() {
        alert('Error: Request Timed Out. Please inform an administrator of this issue.');
      };

      var formData = new FormData();
      if(recording.type === 'video') {
        formData.append('video', recording.video.blob, 'video');
      }
      formData.append('audio', recording.audio.blob, 'video');

      xhr.upload.onprogress = function(event) {
        var progress = Math.round(event.lengthComputable ? event.loaded * 100 / event.total : 0);
        recording.uploadProgress = progress;
        window.top.postMessage({channel: 'recording.uploadProgress', recording: recording}, '*');
      };

      xhr.onload = function() {
        recording.uploaded = true;
        storeLocally();
        window.top.postMessage({channel: 'recording.uploaded', recording: recording}, '*');
        callback ? callback(recording) : null;
      };

      xhr.onerror = function() {
        alert('Error: ' + xhr.status + '. Please inform an administrator of this issue.');
        recording.error = xhr.status;
        callback ? callback(false) : null;
      };

      xhr.onabort = function() {
        recording.error = xhr.status;
        callback ? callback(false) : null;
      };

      xhr.open('POST', '/api/v1/recording/'+recording.id);

      xhr.send(formData);
    };

    var openRecording = function(recording) {
      window.location.href = getRecordingUrl(recording)+'?hasRecorded=true';
    };

    var getRecordingUrl = function(recording) {
      return '/recording/'+recording.id;
    };

    var createRecording = function(type) {
      return {
        'name': null, // String
        'type': type, // String ('audio' or 'video')
        'id': '', // String
        'deleteId': null, // String
        'date': Date.now(), // Timestamp
        'snapshot': '', // Data URI
        'video': {
          'uri': null, // Data URI
          'blob': null, // Binary blob
        },
        'audio': {
          'uri': null, // Data URI
          'blob': null, // Binary blob
        },
        'fullUrl': null, // Full URL to recording.
        'uploadProgress': 0, // Progress of upload. Between 0 and 100 inclusive.
        'uploaded': false, // Whether or not the recording is uploaded. Bool.
      };
    };

    return {
      getRecordings: getRecordings,
      addRecording: addRecording,
      deleteRecording: deleteRecording,
      createRecording: createRecording,
      storeLocally: storeLocally,
      clearList: clearList,
      prepareUploadRecording: prepareUploadRecording,
      uploadRecording: uploadRecording,
      openRecording: openRecording,
      getRecordingUrl: getRecordingUrl,
    };
  }]);
};

module.exports = serviceFactory;
