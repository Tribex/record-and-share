// COPYRIGHT (c) Joshua Bemenderfer

var serviceFactory = function(app) {
  app.service('StateService', ['$rootScope', function($rootScope) {
    var $scope = this;
    $scope.state = 'default';

    var getState = function() {
      return $scope.state;
    };

    var setState = function(state) {
      $scope.state = state;
      $rootScope.$broadcast('stateChange', state);
    };

    return {
      getState: getState,
      setState: setState,
    };
  }]);
};

module.exports = serviceFactory;
