/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	// COPYRIGHT (c) Joshua Bemenderfer 2015

	var app = angular.module('RecordAndShareApp', ['ngMaterial', 'timer'])
	.config(function($mdGestureProvider) {
	  $mdGestureProvider.skipClickHijack();
	});

	// Load Services
	__webpack_require__(1)(app);
	__webpack_require__(2)(app);
	// Load Components
	__webpack_require__(3)(app);
	__webpack_require__(4)(app);
	__webpack_require__(5)(app);

	app.config(function($mdThemingProvider) {
	  $mdThemingProvider.theme('default')
	    .primaryPalette('amber')
	    .accentPalette('green');
	});

	var calculateVideoWidth = function() {
	  var videoElement = document.getElementById('videoDisplay');
	  var controlsElement = document.querySelector('#mediaControls .inner');
	  videoElement.width = window.innerWidth;
	  videoElement.height = window.innerHeight-64;
	};

	window.addEventListener('resize', calculateVideoWidth);
	calculateVideoWidth();


/***/ },
/* 1 */
/***/ function(module, exports) {

	// COPYRIGHT (c) Joshua Bemenderfer

	var serviceFactory = function(app) {
	  app.service('StateService', ['$rootScope', function($rootScope) {
	    var $scope = this;
	    $scope.state = 'default';

	    var getState = function() {
	      return $scope.state;
	    };

	    var setState = function(state) {
	      $scope.state = state;
	      $rootScope.$broadcast('stateChange', state);
	    };

	    return {
	      getState: getState,
	      setState: setState,
	    };
	  }]);
	};

	module.exports = serviceFactory;


/***/ },
/* 2 */
/***/ function(module, exports) {

	// COPYRIGHT (c) Joshua Bemenderfer

	var serviceFactory = function(app) {
	  app.service('RecordingService', ['$http', function($http) {
	    var recordings = [];

	    try {
	      var storedRecordings = JSON.parse(localStorage.getItem('recordings'));
	      if(Array.isArray(storedRecordings)) {
	        recordings = storedRecordings;
	      }
	    } catch (e) {
	      console.log('Resetting Local Storage');
	    }

	    var getRecordings = function() {
	      return recordings;
	    };

	    var addRecording = function(recording) {
	      recordings.push(recording);
	    };

	    var deleteRecording = function(recording) {
	      if(recording.uploaded) {
	        $http({
	          url: '/api/v1/recording/'+recording.deleteId,
	          method: 'DELETE',
	        }).then(function(response) {
	          if(response.status === 200 && response.data && response.data.status === 'success') {
	            recordings.splice(recordings.indexOf(recording), 1);
	            storeLocally();
	          }
	        }, function(response) {
	          // If we get a 404, then the recording didn't exist in the first place, so delete it locally.
	          if(response.status === 404) {
	            recordings.splice(recordings.indexOf(recording), 1);
	            storeLocally();
	          }
	        });
	      }
	    };

	    var storeLocally = function() {
	      localStorage.setItem('recordings', JSON.stringify(recordings));
	    };

	    var clearList = function() {
	      recordings.length = 0;
	      storeLocally();
	    };

	    // TODO: Implement preparation logic.
	    var prepareUploadRecording = function(recording, callback) {
	      $http({
	        url: '/api/v1/recording',
	        method: 'POST',
	        data: {
	          name: recording.name,
	          type: recording.type,
	          snapshot: recording.snapshot,
	          date: recording.date,
	        },
	      }).then(function(response) {
	        if(response.status === 200) {
	          recording.id = response.data.id;
	          recording.deleteId = response.data.deleteId;
	          recording.fullUrl = (window.location + getRecordingUrl(recording)).split('?')[0]
	          // Ehehe...
	          .replace('//recording', '/recording');

	          window.top.postMessage({channel: 'recording.idReady', recording: recording}, '*');

	          callback ? callback(recording) : null;
	        } else {
	          callback ? callback(false, response.status) : null;
	        }
	      }, function(errResponse) {
	        callback ? callback(false, response.status) : null;
	      });
	    };

	    var uploadRecording = function(recording, callback) {
	      window.top.postMessage({channel: 'recording.creationStarted', recording: recording}, '*');
	      var xhr = new XMLHttpRequest();
	      xhr.timeout = 14400000;

	      xhr.ontimeout = function() {
	        alert('Error: Request Timed Out. Please inform an administrator of this issue.');
	      };

	      var formData = new FormData();
	      if(recording.type === 'video') {
	        formData.append('video', recording.video.blob, 'video');
	      }
	      formData.append('audio', recording.audio.blob, 'video');

	      xhr.upload.onprogress = function(event) {
	        var progress = Math.round(event.lengthComputable ? event.loaded * 100 / event.total : 0);
	        recording.uploadProgress = progress;
	        window.top.postMessage({channel: 'recording.uploadProgress', recording: recording}, '*');
	      };

	      xhr.onload = function() {
	        recording.uploaded = true;
	        storeLocally();
	        window.top.postMessage({channel: 'recording.uploaded', recording: recording}, '*');
	        callback ? callback(recording) : null;
	      };

	      xhr.onerror = function() {
	        alert('Error: ' + xhr.status + '. Please inform an administrator of this issue.');
	        recording.error = xhr.status;
	        callback ? callback(false) : null;
	      };

	      xhr.onabort = function() {
	        recording.error = xhr.status;
	        callback ? callback(false) : null;
	      };

	      xhr.open('POST', '/api/v1/recording/'+recording.id);

	      xhr.send(formData);
	    };

	    var openRecording = function(recording) {
	      window.location.href = getRecordingUrl(recording)+'?hasRecorded=true';
	    };

	    var getRecordingUrl = function(recording) {
	      return '/recording/'+recording.id;
	    };

	    var createRecording = function(type) {
	      return {
	        'name': null, // String
	        'type': type, // String ('audio' or 'video')
	        'id': '', // String
	        'deleteId': null, // String
	        'date': Date.now(), // Timestamp
	        'snapshot': '', // Data URI
	        'video': {
	          'uri': null, // Data URI
	          'blob': null, // Binary blob
	        },
	        'audio': {
	          'uri': null, // Data URI
	          'blob': null, // Binary blob
	        },
	        'fullUrl': null, // Full URL to recording.
	        'uploadProgress': 0, // Progress of upload. Between 0 and 100 inclusive.
	        'uploaded': false, // Whether or not the recording is uploaded. Bool.
	      };
	    };

	    return {
	      getRecordings: getRecordings,
	      addRecording: addRecording,
	      deleteRecording: deleteRecording,
	      createRecording: createRecording,
	      storeLocally: storeLocally,
	      clearList: clearList,
	      prepareUploadRecording: prepareUploadRecording,
	      uploadRecording: uploadRecording,
	      openRecording: openRecording,
	      getRecordingUrl: getRecordingUrl,
	    };
	  }]);
	};

	module.exports = serviceFactory;


/***/ },
/* 3 */
/***/ function(module, exports) {

	// COPYRIGHT (c) Joshua Bemenderfer 2015

	var controllerFactory = function(app) {

	  app.controller('RecentlyUploadedController', ['RecordingService', '$http', function(RecordingService, $http) {
	    this.recordings = RecordingService.getRecordings();

	    this.clearList = RecordingService.clearList;

	    this.openRecording = RecordingService.openRecording;

	    this.deleteRecording = RecordingService.deleteRecording;
	  }]);
	};

	module.exports = controllerFactory;


/***/ },
/* 4 */
/***/ function(module, exports) {

	// COPYRIGHT (c) Joshua Bemenderfer 2015

	var controllerFactory = function(app) {

	  app.controller('SidebarContoller', ['$mdSidenav', function($mdSidenav) {
	    this.isOpen = false;

	    this.toggleOpen = function() {
	      $mdSidenav('left').toggle();
	      this.isOpen = !this.isOpen;
	    };
	  }]);
	};

	module.exports = controllerFactory;


/***/ },
/* 5 */
/***/ function(module, exports) {

	// COPYRIGHT (c) Joshua Bemenderfer 2015

	var controllerFactory = function(app) {

	  app.controller('PlaybackController', ['$scope', '$http', 'StateService', 'RecordingService', function($scope, $http, StateService, RecordingService) {
	    $scope.recording = null;
	    $scope.error = false;
	    $scope.mediaFormat = null;
	    $scope.controlState = 'stopped';
	    $scope.isFramed = (window.top !== window.self);
	    $scope.recordingUrl = window.location.href.split('?')[0];

	    var splitLocation = window.location.href.split('/');
	    var indexOfRecording = splitLocation.indexOf('recording');
	    var query = splitLocation[indexOfRecording+1].split('?')[1];
	    var recordingId = splitLocation[indexOfRecording+1].split('?')[0];

	    $scope.hasRecorded = query && query.length && query.indexOf('hasRecorded=true') !== -1;

	    var controlsElement = null;
	    $http.get('/api/v1/recording/'+recordingId).then(function(data) {
	      $scope.recording = data.data;

	      // Setup copy button.
	      new window.Clipboard('.copy-button');

	      controlsElement = document.getElementById('videoDisplay');

	      // Lazy error check to switch the video to WebM if Mp4 isn't supported.
	      controlsElement.addEventListener('error', function() {
	        if($scope.mediaFormate !== 'WebM') {
	          $scope.setMediaFormat('WebM');
	        } else {
	          $scope.setMediaFormat('MP4');
	        }
	      }, true);

	      $scope.setMediaFormat(window.chrome ? 'WebM' : 'MP4');

	      controlsElement.addEventListener('ended', function() {
	        $scope.$broadcast('timer-start');
	        $scope.$broadcast('timer-stop');
	        $scope.controlState = 'stopped';
	        $scope.$digest();
	      });

	      //$scope.playRecording();
	    }, function(err) {
	      console.log(err.status);
	      $scope.error = err.data.error || err.status;
	    });

	    $scope.setMediaFormat = function(format) {
	      $scope.mediaFormat = format;
	      if(format === 'WebM') {
	        controlsElement.src = '/api/v1/recording/'+recordingId+'/recording.webm';
	      } else {
	        controlsElement.src = '/api/v1/recording/'+recordingId+'/recording.mp4';
	      }
	    };

	    $scope.playRecording = function() {
	      $scope.$broadcast($scope.controlState === 'paused' ? 'timer-resume' : 'timer-start');
	      $scope.controlState = 'playing';
	      controlsElement.play();
	    };

	    $scope.stopRecording = function() {
	      $scope.$broadcast('timer-start');
	      $scope.$broadcast('timer-stop');
	      $scope.controlState = 'stopped';
	      controlsElement.currentTime = 0;
	      controlsElement.pause();
	    };

	    $scope.pauseRecording = function() {
	      $scope.$broadcast('timer-stop');
	      $scope.controlState = 'paused';
	      controlsElement.pause();
	    };

	    $scope.insertRecording = function() {
	      $scope.recording.fullUrl = window.location.href;
	      // Tell the embedding page that the user has requested to insert the recording into itself.
	      window.top.postMessage({channel: 'recording.insertRecording', recording: $scope.recording}, '*');
	    };

	    $scope.goHome = function() {
	      window.location = '/';
	    };
	  }]);
	};

	module.exports = controllerFactory;


/***/ }
/******/ ]);