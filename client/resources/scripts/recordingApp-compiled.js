/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	// COPYRIGHT (c) Joshua Bemenderfer 2015

	var app = angular.module('RecordAndShareApp', ['ngMaterial', 'timer'])
	.config(function($mdGestureProvider) {
	  $mdGestureProvider.skipClickHijack();
	});

	// Load Services
	__webpack_require__(1)(app);
	__webpack_require__(2)(app);
	// Load Components
	__webpack_require__(3)(app);
	__webpack_require__(4)(app);
	__webpack_require__(5)(app);
	__webpack_require__(8)(app);

	app.controller('DialogController', [
	  '$scope', 'StateService', '$mdDialog', '$mdMedia',
	  function($scope, StateService, $mdDialog, $mdMedia) {

	    $scope.showUploadDialog = function(ev) {
	      var useFullScreen = $mdMedia('xs') || $mdMedia('sm');
	      $mdDialog.show({
	        controller: 'UploadDialogController',
	        templateUrl: 'views/components/upload-dialog.html',
	        parent: angular.element(document.body),
	        targetEvent: ev,
	        clickOutsideToClose: false,
	        fullscreen: useFullScreen,
	      })
	      .then(function(answer) {
	        $scope.status = 'You said the information was "' + answer + '".';
	      }, function() {
	        $scope.status = 'You cancelled the dialog.';
	      });
	    };

	    $scope.$on('stateChange', function(channel, message) {
	      if(message === 'hasRecorded') {
	        $scope.showUploadDialog();
	      }
	    });

	  }]
	);

	app.config(function($mdThemingProvider) {
	  $mdThemingProvider.theme('default')
	    .primaryPalette('amber')
	    .accentPalette('green');
	});

	var calculateVideoWidth = function() {
	  var videoElement = document.getElementById('videoDisplay');
	  videoElement.width = window.innerWidth;
	  videoElement.height = window.innerHeight-64;
	};

	window.addEventListener('resize', calculateVideoWidth);
	calculateVideoWidth();


/***/ },
/* 1 */
/***/ function(module, exports) {

	// COPYRIGHT (c) Joshua Bemenderfer

	var serviceFactory = function(app) {
	  app.service('StateService', ['$rootScope', function($rootScope) {
	    var $scope = this;
	    $scope.state = 'default';

	    var getState = function() {
	      return $scope.state;
	    };

	    var setState = function(state) {
	      $scope.state = state;
	      $rootScope.$broadcast('stateChange', state);
	    };

	    return {
	      getState: getState,
	      setState: setState,
	    };
	  }]);
	};

	module.exports = serviceFactory;


/***/ },
/* 2 */
/***/ function(module, exports) {

	// COPYRIGHT (c) Joshua Bemenderfer

	var serviceFactory = function(app) {
	  app.service('RecordingService', ['$http', function($http) {
	    var recordings = [];

	    try {
	      var storedRecordings = JSON.parse(localStorage.getItem('recordings'));
	      if(Array.isArray(storedRecordings)) {
	        recordings = storedRecordings;
	      }
	    } catch (e) {
	      console.log('Resetting Local Storage');
	    }

	    var getRecordings = function() {
	      return recordings;
	    };

	    var addRecording = function(recording) {
	      recordings.push(recording);
	    };

	    var deleteRecording = function(recording) {
	      if(recording.uploaded) {
	        $http({
	          url: '/api/v1/recording/'+recording.deleteId,
	          method: 'DELETE',
	        }).then(function(response) {
	          if(response.status === 200 && response.data && response.data.status === 'success') {
	            recordings.splice(recordings.indexOf(recording), 1);
	            storeLocally();
	          }
	        }, function(response) {
	          // If we get a 404, then the recording didn't exist in the first place, so delete it locally.
	          if(response.status === 404) {
	            recordings.splice(recordings.indexOf(recording), 1);
	            storeLocally();
	          }
	        });
	      }
	    };

	    var storeLocally = function() {
	      localStorage.setItem('recordings', JSON.stringify(recordings));
	    };

	    var clearList = function() {
	      recordings.length = 0;
	      storeLocally();
	    };

	    // TODO: Implement preparation logic.
	    var prepareUploadRecording = function(recording, callback) {
	      $http({
	        url: '/api/v1/recording',
	        method: 'POST',
	        data: {
	          name: recording.name,
	          type: recording.type,
	          snapshot: recording.snapshot,
	          date: recording.date,
	        },
	      }).then(function(response) {
	        if(response.status === 200) {
	          recording.id = response.data.id;
	          recording.deleteId = response.data.deleteId;
	          recording.fullUrl = (window.location + getRecordingUrl(recording)).split('?')[0]
	          // Ehehe...
	          .replace('//recording', '/recording');

	          window.top.postMessage({channel: 'recording.idReady', recording: recording}, '*');

	          callback ? callback(recording) : null;
	        } else {
	          callback ? callback(false, response.status) : null;
	        }
	      }, function(errResponse) {
	        callback ? callback(false, response.status) : null;
	      });
	    };

	    var uploadRecording = function(recording, callback) {
	      window.top.postMessage({channel: 'recording.creationStarted', recording: recording}, '*');
	      var xhr = new XMLHttpRequest();
	      xhr.timeout = 14400000;

	      xhr.ontimeout = function() {
	        alert('Error: Request Timed Out. Please inform an administrator of this issue.');
	      };

	      var formData = new FormData();
	      if(recording.type === 'video') {
	        formData.append('video', recording.video.blob, 'video');
	      }
	      formData.append('audio', recording.audio.blob, 'video');

	      xhr.upload.onprogress = function(event) {
	        var progress = Math.round(event.lengthComputable ? event.loaded * 100 / event.total : 0);
	        recording.uploadProgress = progress;
	        window.top.postMessage({channel: 'recording.uploadProgress', recording: recording}, '*');
	      };

	      xhr.onload = function() {
	        recording.uploaded = true;
	        storeLocally();
	        window.top.postMessage({channel: 'recording.uploaded', recording: recording}, '*');
	        callback ? callback(recording) : null;
	      };

	      xhr.onerror = function() {
	        alert('Error: ' + xhr.status + '. Please inform an administrator of this issue.');
	        recording.error = xhr.status;
	        callback ? callback(false) : null;
	      };

	      xhr.onabort = function() {
	        recording.error = xhr.status;
	        callback ? callback(false) : null;
	      };

	      xhr.open('POST', '/api/v1/recording/'+recording.id);

	      xhr.send(formData);
	    };

	    var openRecording = function(recording) {
	      window.location.href = getRecordingUrl(recording)+'?hasRecorded=true';
	    };

	    var getRecordingUrl = function(recording) {
	      return '/recording/'+recording.id;
	    };

	    var createRecording = function(type) {
	      return {
	        'name': null, // String
	        'type': type, // String ('audio' or 'video')
	        'id': '', // String
	        'deleteId': null, // String
	        'date': Date.now(), // Timestamp
	        'snapshot': '', // Data URI
	        'video': {
	          'uri': null, // Data URI
	          'blob': null, // Binary blob
	        },
	        'audio': {
	          'uri': null, // Data URI
	          'blob': null, // Binary blob
	        },
	        'fullUrl': null, // Full URL to recording.
	        'uploadProgress': 0, // Progress of upload. Between 0 and 100 inclusive.
	        'uploaded': false, // Whether or not the recording is uploaded. Bool.
	      };
	    };

	    return {
	      getRecordings: getRecordings,
	      addRecording: addRecording,
	      deleteRecording: deleteRecording,
	      createRecording: createRecording,
	      storeLocally: storeLocally,
	      clearList: clearList,
	      prepareUploadRecording: prepareUploadRecording,
	      uploadRecording: uploadRecording,
	      openRecording: openRecording,
	      getRecordingUrl: getRecordingUrl,
	    };
	  }]);
	};

	module.exports = serviceFactory;


/***/ },
/* 3 */
/***/ function(module, exports) {

	// COPYRIGHT (c) Joshua Bemenderfer 2015

	var controllerFactory = function(app) {

	  app.controller('RecentlyUploadedController', ['RecordingService', '$http', function(RecordingService, $http) {
	    this.recordings = RecordingService.getRecordings();

	    this.clearList = RecordingService.clearList;

	    this.openRecording = RecordingService.openRecording;

	    this.deleteRecording = RecordingService.deleteRecording;
	  }]);
	};

	module.exports = controllerFactory;


/***/ },
/* 4 */
/***/ function(module, exports) {

	// COPYRIGHT (c) Joshua Bemenderfer 2015

	var controllerFactory = function(app) {

	  app.controller('SidebarContoller', ['$mdSidenav', function($mdSidenav) {
	    this.isOpen = false;

	    this.toggleOpen = function() {
	      $mdSidenav('left').toggle();
	      this.isOpen = !this.isOpen;
	    };
	  }]);
	};

	module.exports = controllerFactory;


/***/ },
/* 5 */
/***/ function(module, exports, __webpack_require__) {

	// COPYRIGHT (c) Joshua Bemenderfer 2015

	var VideoRecorder = __webpack_require__(6);
	var AudioRecorder = __webpack_require__(7);

	var controllerFactory = function(app) {

	  app.controller('RecorderController', ['StateService', 'RecordingService', function(StateService, RecordingService) {
	    var $scope = this;
	    $scope.isRecording = false;
	    $scope.recordingType = null; // 'audio' | 'video'
	    $scope.recordings = RecordingService.getRecordings();
	    $scope.isFramed = (window.top !== window.self);
	    $scope.isOnIOS = false;

	    $scope.currentRecording;

	    var videoElement = document.getElementById('videoDisplay');
	    var offscreenCanvas = document.getElementById('offscreenCanvas');
	    var videoRecorder, audioRecorder;

	    // Handle any recording preparation logic here.
	    var successCallback = function(stream) {
	      $scope.currentRecording = RecordingService.createRecording($scope.recordingType);
	      $scope.isRecording = true;


	      RecordingService.prepareUploadRecording($scope.currentRecording,
	        // TODO: More robust error handling.
	        function(recording, status) {
	          if(!recording) {
	            $scope.error = status;
	          }
	        });

	      audioRecorder = new AudioRecorder(stream);

	      if($scope.recordingType === 'video') {
	        videoRecorder = new VideoRecorder(stream);
	      }

	      audioRecorder.initialize(function() {
	        if(videoRecorder) {
	          videoRecorder.initialize(function() {
	            // Both recorders are ready to record things accurately
	            videoRecorder.start();
	            audioRecorder.start();
	          });
	        } else {
	          audioRecorder.start();
	        }
	      });

	      if($scope.recordingType === 'video') {
	        videoElement.addEventListener('loadeddata', function() {
	          // Draw thumbnail, scaled to 100x100 px.
	          offscreenCanvas.width = 100;
	          offscreenCanvas.height = 100;
	          var context = offscreenCanvas.getContext('2d');
	          context.drawImage(videoElement, 0, 0, 100, 100);
	          $scope.currentRecording.snapshot = offscreenCanvas.toDataURL();
	        });
	      }

	      videoElement.src = window.URL.createObjectURL(stream);
	    };

	    var errorCallback = function() {
	      $scope.error = 'Unable to access camera.';
	    };

	    $scope.record = function(type) {
	      if(Modernizr.getusermedia) {
	        var getUserMedia = Modernizr.prefixed('getUserMedia', navigator);

	        var options = {
	          video: type === 'video',
	          audio: true,
	        };

	        $scope.recordingType = type;
	        getUserMedia(options, successCallback, errorCallback);
	      }
	    };

	    $scope.stop = function() {
	      $scope.isRecording = false;
	      videoElement.src = '';

	      RecordingService.addRecording($scope.currentRecording);
	      StateService.setState('hasRecorded');
	      var saveRecording = function(type, uri, blob) {
	        $scope.currentRecording[type] = {
	          uri: uri,
	          blob: blob,
	        };

	        if($scope.recordingType === 'audio' || $scope.currentRecording['video'].blob && $scope.currentRecording['audio'].blob) {
	          RecordingService.uploadRecording($scope.currentRecording);
	        }
	      };

	      if($scope.recordingType === 'video') {
	        videoRecorder.stop(function (videoURI) {
	          var videoBlob = videoRecorder.getBlob();

	          saveRecording('video', videoURI, videoBlob);
	        });
	      }

	      audioRecorder.stop(function (audioURI) {
	        var audioBlob = audioRecorder.getBlob();

	        saveRecording('audio', audioURI, audioBlob);
	      });
	    };
	  }]);
	};

	module.exports = controllerFactory;


/***/ },
/* 6 */
/***/ function(module, exports) {

	
	var VideoRecorder = function(stream, options) {
	  options = options ? options : {};

	  options.type = 'video';
	  options.mimeType = 'video/webm';
	  options.videoBitsPerSecond = 128000;
	  options.bufferSize = 16384;
	  options.width = 640;
	  options.height = 480;


	  this.recorder = RecordRTC(stream, options);

	  // TODO = Flesh these out as needed.
	  this.initialize = this.recorder.initRecorder;
	  this.start = this.recorder.startRecording;
	  this.stop = this.recorder.stopRecording;

	  this.getBlob = this.recorder.getBlob;
	  this.getDataURL = this.recorder.getDataURL;
	};

	module.exports = VideoRecorder;


/***/ },
/* 7 */
/***/ function(module, exports) {

	
	var AudioRecorder = function(stream, options) {
	  options = options ? options : {};

	  options.type = 'audio';
	  options.mimeType = 'audio/wav';
	  options.audioBitsPerSecond = 128000;
	  options.bufferSize = 16384;
	  options.numberOfAudioChannels = 1;

	  this.recorder = RecordRTC(stream, options);

	  // TODO: Flesh these out as needed.
	  this.initialize = this.recorder.initRecorder;
	  this.start = this.recorder.startRecording;
	  this.stop = this.recorder.stopRecording;

	  this.getBlob = this.recorder.getBlob;
	  this.getDataURL = this.recorder.getDataURL;
	};

	module.exports = AudioRecorder;


/***/ },
/* 8 */
/***/ function(module, exports) {

	// COPYRIGHT (c) Joshua Bemenderfer 2015

	var controllerFactory = function(app) {
	  app.controller('UploadDialogController', [
	    '$scope', 'RecordingService', 'StateService', '$mdDialog', '$mdMedia', '$timeout',
	    function($scope, RecordingService, StateService, $mdDialog, $mdMedia, $timeout) {
	      console.log('Initialized');
	      var recordings = RecordingService.getRecordings();
	      var recording = recordings[recordings.length-1];
	      $scope.recording = recording;
	      $scope.$mdMedia = $mdMedia;
	      $scope.isFramed = (window.top !== window.self);

	      $timeout(function() {
	        new window.Clipboard('.copy-button');
	      }, 100);

	      var checkInterval = setInterval(function() {
	        $scope.$digest();
	      }, 200);

	      $scope.openRecording = RecordingService.openRecording;

	      $scope.insertRecording = function() {
	        // Tell the embedding page that the user has requested to insert the recording into itself.
	        window.top.postMessage({channel: 'recording.insertRecording', recording: $scope.recording}, '*');
	      };

	      $scope.deleteRecording = function(recordingToDelete, cancelExt) {
	        RecordingService.deleteRecording(recordingToDelete);
	        $mdDialog.cancel();
	        StateService.setState('default');
	        clearInterval(checkInterval);

	        if(cancelExt) {
	          // Tell the embedding page to close itself because the user has requested to fully cancel the operation.
	          window.top.postMessage({channel: 'recording.fullCancel', recording: $scope.recording}, '*');
	        }
	      };

	      $scope.cancel = function() {
	        // Tell the embedding page that we just canceled the recording.
	        window.top.postMessage({channel: 'recording.dialogCancel', recording: $scope.recording}, '*');

	        if(!$scope.recording.uploaded) {
	          RecordingService.deleteRecording($scope.recording);
	        }
	        $mdDialog.cancel();
	        StateService.setState('default');
	        clearInterval(checkInterval);
	      };

	      // TODO: Implement
	      $scope.upload = function() {
	        RecordingService.uploadRecording($scope.recording);
	        StateService.setState('upload');
	        clearInterval(checkInterval);
	      };
	    }]
	  );
	};

	module.exports = controllerFactory;


/***/ }
/******/ ]);