// COPYRIGHT (c) 2015 Joshua Bemenderfer

'use strict';

// v1/id-manager.js - Handles generation and access of random date-based ids.

var Chance = require('chance');

let idManager = {
  getIds: function(date) {
    var seedGenerator = new Chance(date);

    return {
      id: seedGenerator.string({
        length: 16,
        pool: 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789',
      }),
      deleteId: seedGenerator.string({
        length: 32,
        pool: 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789',
      }),
    };
  },
};

module.exports = idManager;
