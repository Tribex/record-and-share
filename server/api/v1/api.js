// COPYRIGHT (c) 2015 Joshua Bemenderfer

'use strict';

// v1/api.js - API Version 1 Main file.

let express = require('express');
let bodyParser = require('body-parser');
let multer = require('multer');
let router = express.Router();
let fs = require('fs');
let path = require('path');
let mkdirp = require('mkdirp');
let rimraf = require('rimraf');
let ffmpeg = require('fluent-ffmpeg');

let Logger = require('../../util/logger');
let ConfigManager = require('../../util/config-manager');
let DatabaseManager = require('../../database/database-manager');
let idManager = require('./id-manager');

let storageDir = ConfigManager.mainConfig.get()['storage-dir'];
// Multer configuration.
let storage = multer.diskStorage({
  destination: function (req, file, cb) {
    // TODO: Error handling.
    let pathName = path.join(storageDir, req.params.id);
    mkdirp.sync(pathName);
    cb(null, pathName);
  },
  filename: function (req, file, cb) {
    let filename = file.fieldname + '.' + file.mimetype.split('/')[1];
    cb(null, filename);
  },
});

let uploader = multer({
  storage: storage,
  limits: {fileSize: ConfigManager.mainConfig.get()['max-recording-size']},
});

let incompleteRecordings = {};

router.route('/recording')
.post(bodyParser.json(), function(req, res) {
  // Generate unique Ids based on date.
  let idPair = idManager.getIds(req.body.date);

  DatabaseManager.getRecording(idPair.id, false, function(data) {
    if(!data) {
      req.body.id = idPair.id; // Recording identifier for public access. 16 characters.
      req.body.deleteId = idPair.deleteId; // Recording identifier for deletion. 32 characters.

      res.send({
        id: idPair.id,
        deleteId: idPair.deleteId,
      });

      incompleteRecordings[idPair.id] = req.body;

      Logger.info('Created new incomplete recording %s', idPair.id);
    } else {
      // Should never happen except in tetsing, as the seeds are unique.
      Logger.warn('Recording "'+idPair.id+'" already exists. Client attempted duplicate upload');
      res.status(409);
      res.send({error: 'Recording "'+idPair.id+'" already exists. Client attempted duplicate upload'});
    }
  });
});

router.route('/recording/:id')
// Recording Metadata Requests
.get(function(req, res) {
  DatabaseManager.getRecording(req.params.id, false, function(data) {
    if(data) {
      Logger.info('Sent recording "'+req.params.id+'" to client');
      res.send(data);
      // Handle incomplete recordings.
    } else if (incompleteRecordings[req.params.id]) {
      // Inform the client that the recording is not ready yet.
      Logger.info('Sent partially-uploaded recording metadata "'+req.params.id+'" to client');
      res.status(202);
      res.send(data);
    } else {
      Logger.info('Client requested invalid recording "'+req.params.id+'"');
      res.status(404);
      res.send({error: 'Recording "'+req.params.id+'" Not Found'});
    }
  });
})
// Recording Video+Audio Uploads
.post(uploader.fields([
  { name: 'video', maxCount: 1 },
  { name: 'audio', maxCount: 1 },
]), function(req, res) {
  if(incompleteRecordings[req.params.id]) {
    // Save recording into database since the full contents have been uploaded.
    DatabaseManager.addRecording(incompleteRecordings[req.params.id], function(result) {
      Logger.info('Inserted new recording %s into database', req.params.id);
    });

    // Reomve id from incomplete recordings dict so no more files can be uploaded to it.
    let recording = incompleteRecordings[req.params.id];
    incompleteRecordings[req.params.id] = null;
    delete incompleteRecordings[req.params.id];

    let mp4Encoded = false;
    let webmEncoded = false;
    let encodeCallback = function(format) {
      if(format === 'mp4') {
        mp4Encoded = true;

        // We can exit safely after mp4 is encoded.
        res.status(200);
        res.send({status: 'success'});
      }

      else if(format === 'webm') webmEncoded = true;

      if(mp4Encoded && webmEncoded) {
        Logger.info('Finished transcoding file for recording %s', req.params.id);

        // Delete source files to save space.
        if(recording.type === 'video') {
          fs.unlink(path.join(storageDir, req.params.id, 'video.webm'));
        }
        fs.unlink(path.join(storageDir, req.params.id, 'audio.wav'));
      }
    };

    // Primary mp4 encoding.
    let mp4Pipeline = ffmpeg(path.join(storageDir, req.params.id, 'audio.wav'));
    if(recording.type === 'video') {
      mp4Pipeline.input(path.join(storageDir, req.params.id, 'video.webm'));
    }
    mp4Pipeline.outputOptions('-strict -2');
    mp4Pipeline.on('error', (e, stdout, stderr) => Logger.error(stderr));

    mp4Pipeline.save(path.join(storageDir, req.params.id, 'combined.mp4')).format('mp4').on('end', function() {
      encodeCallback('mp4');
    });

    let webmPipeline = ffmpeg(path.join(storageDir, req.params.id, 'audio.wav'));
    if(recording.type === 'video') {
      webmPipeline.input(path.join(storageDir, req.params.id, 'video.webm'));
    }
    webmPipeline.on('error', (e, stdout, stderr) => Logger.error(stderr));

    // Secondary webm encoding.
    webmPipeline.save(path.join(storageDir, req.params.id, 'combined.webm')).format('webm').on('end', function() {
      encodeCallback('webm');
    });
  } else {
    res.status(404);
    res.send({error: 'Recording "'+req.params.id+'" Not Found or Invalid'});
  }
})
// Recording Deletion
.delete(function(req, res) {
  DatabaseManager.getRecording(req.params.id, true, function(recording) {
    DatabaseManager.deleteRecording(req.params.id, function(data) {
      if(data && data.result && data.result.n === 1) {
        rimraf(path.join(storageDir, recording.id), function() {
          Logger.info('Client deleted recording with deleteId "'+req.params.id+'"');
          res.send({
            status: 'success',
          });
        });
      } else {

        // Delete any matching incomplete recordings.
        let incompleteRecordingKeys = Object.keys(incompleteRecordings);
        for(let i = 0; i < incompleteRecordingKeys.length; i++) {
          let incompleteRecording = incompleteRecordings[incompleteRecordingKeys[i]];

          if(incompleteRecording.deleteId === req.params.id) {
            Logger.info('Client deleted incomplete recording with deleteId "'+req.params.id);
            res.send({
              status: 'success',
            });
          }
          return;
        }

        // Otherwise it was an invalid recording.
        Logger.info('Client attempted to delete invalid recording "'+req.params.id+'"');
        res.status(404);
        res.send({'error': 'Recording "'+req.params.id+'" Not Found'});
      }
    });
  });
});

// Access Recording Video
router.get('/recording/:id/recording.mp4', function(req, res) {
  try {
    res.sendFile(path.resolve(storageDir, req.params.id, 'combined.mp4'));
  } catch(e) {
    res.status(404);
    res.send({'error': 'Recording "'+req.params.id+'" Not Found or Processing'});
  }
});

router.get('/recording/:id/recording.webm', function(req, res) {
  try {
    res.sendFile(path.resolve(storageDir, req.params.id, 'combined.webm'));
  } catch(e) {
    res.status(404);
    res.send({'error': 'Recording "'+req.params.id+'" Not Found or Processing'});
  }
});

module.exports = router;
