#!/usr/bin/env node
// COPYRIGHT (c) 2015 Joshua Bemenderfer
'use strict';

// Load dependencies.
let fs = require('fs');
let path = require('path');
let https = require('https');
let express = require('express');
let helmet = require('helmet');
let ConfigManager = require('./util/config-manager');
let Logger = require('./util/logger');

// Resets configuration to defaults. Be careful.
if(process.argv[2] === 'reset-config') {
  ConfigManager.mainConfig.reset();
}

let API = require('./api/v1/api.js');

// Set up global client & server configuration.
let Instance = express();

// Set up instance additional properties (Not best practice)
Instance.ClientDir = path.join(__dirname, ConfigManager.mainConfig.get()['client-dir']);

Instance.use(express.static(Instance.ClientDir, {maxAge: 31557600000}));

// Basic Security Measures.
Instance.use(helmet.hidePoweredBy());
Instance.use(helmet.ieNoOpen());
Instance.use(helmet.hsts());
Instance.use(helmet.noSniff());
Instance.use(helmet.xssFilter());

Instance.get('/', function(req, res) {
  res.sendFile(path.join(__dirname, ConfigManager.mainConfig.get()['client-dir'], 'views/index.html'));
});

Instance.get('/recording/:id', function(req, res) {
  res.sendFile(path.join(__dirname, ConfigManager.mainConfig.get()['client-dir'], 'views/playback.html'));
});

Instance.get('/.htaccess', function(req, res) {
  res.sendFile(path.join(__dirname, '.htaccess'));
});

Instance.use('/api/v1', API);

try {
  let HTTPSServer = https.createServer({
    key: fs.readFileSync('./data/ssl/key.pem'),
    cert: fs.readFileSync('./data/ssl/cert.pem'),
  }, Instance);

  HTTPSServer.listen(+ConfigManager.mainConfig.get().ports.https);
} catch (e) {
  Logger.warn('HTTPS Server Disabled: '+e.message);
}

let Server = Instance.listen(+ConfigManager.mainConfig.get().ports.http, function () {
  let port = Server.address().port;

  Logger.info('Server started on port %s', port);
});
