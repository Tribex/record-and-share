// COPYRIGHT (c) 2015 Joshua Bemenderfer

'use strict';

// database/mongo-operations.js - Abstracts mongo database interactions.
var ConfigManager = require('../util/config-manager');
var Logger = require('../util/logger');
var MongoClient = require('mongodb').MongoClient;

var DatabaseManager = {};
DatabaseManager.db = null;
DatabaseManager.dburl = ConfigManager.mainConfig.get()['mongo-url'];

DatabaseManager.init = function() {
  MongoClient.connect(DatabaseManager.dburl, function(err, db) {
    if(err) {
      Logger.error(err);
      Logger.error('Database connection failed.');
      DatabaseManager.cleanupAndExit(1);
    }

    Logger.info('Database connection established.');

    DatabaseManager.db = db;
    DatabaseManager.recordingsCollection = DatabaseManager.db.collection('recordings');

  });
};

DatabaseManager.addRecording = function(recording, callback) {
  DatabaseManager.recordingsCollection.insertOne(recording, function(err, result) {
    if(err) {
      Logger.error(err);
      return;
    }

    callback ? callback(result) : null;
  });
};

DatabaseManager.getRecording = function(id, isDelete, callback) {
  var selector = isDelete ? {deleteId :id} : {id: id};
  DatabaseManager.recordingsCollection.findOne(selector, function(err, result) {
    if(err) {
      Logger.error(err);
      return;
    }

    callback ? callback(result) : null;
  });
};

DatabaseManager.deleteRecording = function(deleteId, callback) {
  DatabaseManager.recordingsCollection.deleteOne({deleteId: deleteId}, function(err, result) {
    if(err) {
      Logger.error(err);
      return;
    }

    callback ? callback(result) : null;
  });
};

DatabaseManager.cleanupAndExit = function(code) {
  try {
    DatabaseManager.db.close();
  } catch (e) {
    Logger.error('Failed to close MongoDB connection.');
  };
  process.exit(code || 0);
};

// Make sure we don't leak database connections after server shutdown.
process.on('SIGINT', DatabaseManager.cleanupAndExit);
process.on('SIGTERM', DatabaseManager.cleanupAndExit);

DatabaseManager.init();

module.exports = DatabaseManager;
