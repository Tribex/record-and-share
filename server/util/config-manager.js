// COPYRIGHT (c) 2015 Joshua Bemenderfer
'use strict';

// util/config-manager - Basic key-value configuration manager.

let fs = require('fs');

/**
 * Configuration Manager class. Handles saving, loading, manipulating,
 * and resetting of the specified configuration files.
 *
 * @param {String} path The path to the configuration file to be used.
 * @param {String} defaultPath The path to the default configuration file to be used for resets.
 * @param {Boolean} createIfNotExists Whether or not to create the configuration file if it is non-existent.
 * @returns {ConfigManager} An instance of ConfigManager with the above values set.
 */
let ConfigManager = function(path, defaultPath, createIfNotExists) {
  this.path = path;
  this.defaultPath = defaultPath;
  this.config = {};

  this.createIfNotExists = createIfNotExists;

  this.get = function() {
    return this.config;
  };

  /**
   * Resets the configuration to default values.
   *
   * @returns {Boolean} true on success, false on failure.
   */
  this.reset = function() {
    if(this.defaultPath) {
      this.load(this.defaultPath);
      this.save();
      return true;
    }
    return false;
  };

  this.load = function(optPath) {
    let loadPath = optPath ? optPath : this.path;
    try {
      this.config = JSON.parse(fs.readFileSync(loadPath, {encoding: 'utf-8'}));
    } catch (e) {
      throw new Error('Config file '+optPath+' missing or invalid.');
    }
  };

  this.init = function() {
    try {
      this.load();
    } catch (e) {
      if(e.code === 'ENOENT' && this.createIfNotExists) {
        if(this.defaultPath) {
          this.reset();
        } else {
          fs.writeFileSync(this.path, '{}');
          this.config = {};
        }
      }
    }
  };

  this.save = function() {
    fs.writeFileSync(this.path, JSON.stringify(this.config, null, '\t'));
  };

  this.init();
};

ConfigManager.mainConfig = new ConfigManager('data/config.json', 'util/config-defaults/config-default.json', true);

module.exports = ConfigManager;
