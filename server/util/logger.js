// COPYRIGHT (c) 2015 Joshua Bemenderfer
'use strict';

// util/logger - Winston-based logger with multiple file transports.

let winston = require('winston');
let mkdirp = require('mkdirp');
winston.emitErrs = true;

// Initialize Logging Folder
mkdirp.sync('./data/logs');

let Logger = new winston.Logger({
  transports: [
    // Global file logger. JSON-formatted.
    new winston.transports.File({
      handleExceptions: true,
      name: 'info-file',
      level: 'info',
      filename: './data/logs/program.log',
      json: true,
      maxFiles: 5,
      maxsize: 5242880,
      colorize: false,
    }),
    // Debug file logger. JSON-formatted.
    new winston.transports.File({
      handleExceptions: true,
      name: 'debug-file',
      level: 'debug',
      filename: './data/logs/debug.log',
      json: true,
      maxFiles: 5,
      maxsize: 5242880,
      colorize: false,
    }),
    // Error file logger. JSON-formatted.
    new winston.transports.File({
      handleExceptions: true,
      name: 'error-file',
      level: 'warn',
      filename: './data/logs/error.log',
      json: true,
      maxFiles: 5,
      maxsize: 5242880,
      colorize: false,
    }),
    // Console logger. Colorized, not JSON-formatted.
    new winston.transports.Console({
      name: 'debug-console',
      handleExceptions: true,
      level: 'debug',
      json: false,
      colorize: true,
    }),
  ],
  exitOnError: false,
});

Logger.info('Initializing Logger. Logs are stored in data/logs/*.log.');

module.exports = Logger;

// Support writable streams.
module.exports.stream = {
  write: function (message, encoding) {
    Logger.info(message);
  },
};
