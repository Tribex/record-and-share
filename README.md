# VoiceIt Source Code


Here are the instructions for setting up and using the VoiceIt app.

## Set Up:
1. Install MongoDB globally. Start the `mongod` daemon. (Ex: `mongod --dbpath=/mongo-data --port 27017`)
2. Install `ffmpeg` (NOT `avconv`)
3. Install Node.js >= `4.1`.
4. Move these source files to wherever you intend to run the server from.
5. Change to the `server` directory and run `npm install`.
6. Run `./server.js` once. That will create the directories needed for the server to run properly.
7. Copy/Link your SSL certificate to `data/ssl/cert.pem` and your private key to `data/ssl/key.pem`. This will enable the server to use HTTPS, which is **required** for recording.
8. Edit `data/config.json` to your liking for the production server. (See Configuration section)
9. Run `./server.js` again, ideally with some sort of process manager such as [pm2](https://npmjs.com/package/pm2)
10. Test as needed to make sure everything is working, [contact me](mailto:tribex10@gmail.com) if something is not.

## Configuration:
Default Configuration
```json
{
  "ports": {
    "http": "80",
    "https": "443"
  },
  "client-dir": "../client/",
  "storage-dir": "./data/uploads",
  "mongo-url": "mongodb://localhost:27017/record-and-share",
  "max-recording-size": 20000000
}
```
Explanation:

* **Ports:** Ports to run the server on. Production uses 80 for http and 443 for https. Development and testing should probably run on 8000 and 8443.
* **Client Dir:** Directory where the client files are stored. Leave this alone unless you move the client somewhere else.
* **Storage Dir:** Where you'd like recording files stored. Defaults to `server/data/uploads`.
* **Mongo URL:** The Mongo connection URL to your MongoDB database. Allows you to run the database on a different server. Leave this alone unless you change your mongo configuration.
* **Max Recording Size:** The maximum size of files uploaded to the API. Defaults to 20 MB.

Additional configuration options may be requested as needed.

*NOTE: The server will fail to start if invalid configuration options are provided. Be sure to double-check any changes.*

## Logs:
Logs are json-formatted for easy parsing and can be found in `server/data/logs`.

## Continued Deployment/Updates:
Due to the rushed nature of the development, the code is not entirely in an optimal state. Much remains to be commented or reorganized. Should you like that done or updates regularly provided, it would be wise to set up a git-based deployment method.

The repository has already been set up, simply provide me with the production server user's SSH key, (normally `~/.ssh/id_rsa.pub`), and I will enable it to access the git repository. That will allow you to run `git pull` and restart the server for updates.

## Support:
After a month of deployment, any major updates requested ("major" being determined by my estimate of the work required to implement the update) will cost a minimum of USD $25. Bug fixes will always be free unless a large feature is required to fix them.

## Contact:
Name: Joshua Bemenderfer  
Phone: 1-319-536-6016  
E-Mail: [tribex10@gmail.com](mailto:tribex10@gmail.com)
